using UnrealBuildTool;

namespace UnrealBuildTool.Rules
{
    public class VlcMedia : ModuleRules
    {
        public VlcMedia(TargetInfo Target) {
            PrivateIncludePaths.AddRange(
            new string[] {
                "VlcMedia/Private",
                "VlcMedia/Private/Player",
                "VlcMedia/Private/Tracks",
				"VlcMedia/Private/Vlc",
            }
            );

            PrivateIncludePathModuleNames.AddRange(
            new string[] {
                "Media",
            }
            );

            PrivateDependencyModuleNames.AddRange(
            new string[] {
                "Core",
                "RenderCore",
            }
            );

            DynamicallyLoadedModuleNames.AddRange(
            new string[] {
                "Media",
            }
            );

            if (Target.Platform == UnrealTargetPlatform.Linux) {
                PublicAdditionalLibraries.Add("vlc");
                PublicDelayLoadDLLs.Add("libvlc.so");
                PublicDelayLoadDLLs.Add("libvlccore.so");
            }
        }
    }
}
