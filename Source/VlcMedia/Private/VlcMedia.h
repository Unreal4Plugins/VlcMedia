#pragma once

#include "ModuleManager.h"
#include "Runtime/Media/Public/IMediaModule.h"
#include "Runtime/Media/Public/IMediaPlayerFactory.h"
#include "ModuleInterface.h"
#include "ModuleManager.h"

#pragma comment(lib, "libvlc")
#pragma comment(lib, "libvlccore")

class FVlcMediaModule : public IModuleInterface, public IMediaPlayerFactory
{
public:
    /** IModuleInterface implementation */
    virtual void StartupModule() override;
    virtual void ShutdownModule() override;

    /** IMediaPlayerFactory implementation */
    virtual TSharedPtr<IMediaPlayer> CreatePlayer() override;
    virtual const FMediaFileTypes& GetSupportedFileTypes() const override;
    virtual bool SupportsUrl(const FString& Url) const override;

protected:
    bool LoadRequiredLibraries();

private:
    bool Initialized;
    FMediaFileTypes SupportedFileTypes;
    TArray<FString> SupportedUriSchemes;

    libvlc_instance_t *vlc_instance;
};