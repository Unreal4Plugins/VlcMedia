class FVlcMediaPlayer : public IMediaInfo, public IMediaPlayer, public TSharedFromThis<IMediaPlayer>
{
public:
    FVlcMediaPlayer(libvlc_instance_t *Instance);
    ~FVlcMediaPlayer();

    // IMediaInfo interface
    virtual FTimespan GetDuration() const override;
    virtual TRange<float> GetSupportedRates(EMediaPlaybackDirections Direction, bool Unthinned) const override;
    virtual FString GetUrl() const override;
    virtual bool SupportsRate(float Rate, bool Unthinned) const override;
    virtual bool SupportsScrubbing() const override;
    virtual bool SupportsSeeking() const override;

    // IMediaPlayer interface
    virtual void Close() override;
    virtual const TArray<IMediaAudioTrackRef>& GetAudioTracks() const override;
    virtual const TArray<IMediaCaptionTrackRef>& GetCaptionTracks() const override;
    virtual const IMediaInfo& GetMediaInfo() const override;
    virtual float GetRate() const override;
    virtual FTimespan GetTime() const override;
    virtual const TArray<IMediaVideoTrackRef>& GetVideoTracks() const override;
    virtual bool IsLooping() const override;
    virtual bool IsPaused() const override;
    virtual bool IsPlaying() const override;
    virtual bool IsReady() const override;
    virtual bool Open(const FString& Url) override;
    virtual bool Open(const TSharedRef<FArchive, ESPMode::ThreadSafe>& Archive, const FString& OriginalUrl) override;
    virtual bool Seek(const FTimespan& Time) override;
    virtual bool SetLooping(bool Looping) override;
    virtual bool SetRate(float Rate) override;

    DECLARE_DERIVED_EVENT(FVlcMediaPlayer, IMediaPlayer::FOnMediaClosed, FOnMediaClosed);
    virtual FOnMediaClosed& OnClosed() override
    {
        return ClosedEvent;
    }

    DECLARE_DERIVED_EVENT(FVlcMediaPlayer, IMediaPlayer::FOnMediaOpened, FOnMediaOpened);
    virtual FOnMediaOpened& OnOpened() override
    {
        return OpenedEvent;
    }

    DECLARE_DERIVED_EVENT(FVlcMediaPlayer, IMediaPlayer::FOnTracksChanged, FOnTracksChanged);
    virtual FOnTracksChanged& OnTracksChanged() override
    {
        return TracksChangedEvent;
    }
    
    // FVlcMediaPlayer implementation
    bool ChangeStreamState(EMediaStreamType StreamType, int32 ID, bool Enabled);
    
    IMediaAudioTrackPtr& GetCurrentAudioTrack();
    IMediaVideoTrackPtr& GetCurrentVideoTrack();
    IMediaCaptionTrackPtr& GetCurrentCaptionTrack();
    
    TSharedPtr<FArchive, ESPMode::ThreadSafe>& GetArchive();
    TArray<uint8>& GetFrameBuffer();
    
    bool Open(libvlc_media_t *Media);
    
    static void AudioPlay(void* Opaque, const void* Samples, unsigned Count, int64_t Pts);
    static void* VideoLock(void* Opaque, void** Planes);
    static void VideoDisplay(void* Opaque, void* Picture);
	static void PlayerEndReached(const struct libvlc_event_t *event, void *opaque);
    
    static ssize_t BufferRead(void* Opaque, unsigned char* Buffer, size_t Length);
    static int BufferSeek(void* Opaque, uint64_t Offset);
    static void BufferClose(void* Opaque);

private:
    // Unreal-related
    TMap<int32, IMediaAudioTrackRef> AudioTracks;
    TMap<int32, IMediaCaptionTrackRef> CaptionTracks;
    TMap<int32, IMediaVideoTrackRef> VideoTracks;
    
    TArray<IMediaAudioTrackRef> ATracks;
    TArray<IMediaCaptionTrackRef> CTracks;
    TArray<IMediaVideoTrackRef> VTracks;
    
    FTimespan Duration;
    FString MediaUrl;
	bool Loop;

    FOnMediaClosed ClosedEvent;
    FOnMediaOpened OpenedEvent;
    FOnTracksChanged TracksChangedEvent;

    // VLC-related
	libvlc_instance_t *vlc_instance;
    libvlc_media_player_t *vlc_player;
    
    // General
    TSharedPtr<FArchive, ESPMode::ThreadSafe> Archive;
	TArray<uint8> FrameBuffer;
    
    IMediaAudioTrackPtr CurrentAudioTrack;
    IMediaVideoTrackPtr CurrentVideoTrack;
	IMediaCaptionTrackPtr CurrentCaptionTrack;
};

typedef TSharedPtr<FVlcMediaPlayer, ESPMode::ThreadSafe> FVlcMediaPlayerPtr;
typedef TSharedRef<FVlcMediaPlayer, ESPMode::ThreadSafe> FVlcMediaPlayerRef;
