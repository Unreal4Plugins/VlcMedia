#include "VlcMediaPrivatePCH.h"

/* FVlcVideoPlayer constructors
*****************************************************************************/

FVlcMediaPlayer::FVlcMediaPlayer(libvlc_instance_t *Instance)
    : Duration(FTimespan::Zero())
    , Loop(false)
	, vlc_instance(Instance)
    , vlc_player(NULL)
{
    UE_LOG(LogVlcMedia, Log, TEXT("FVlcMediaPlayer::FVlcMediaPlayer(libvlc_instance_t *Instance)"));
}

FVlcMediaPlayer::~FVlcMediaPlayer()
{
    UE_LOG(LogVlcMedia, Log, TEXT("FVlcMediaPlayer::~FVlcMediaPlayer()"));
    Close();
}

/* IMediaInfo interface
 *****************************************************************************/

FTimespan FVlcMediaPlayer::GetDuration() const
{
    UE_LOG(LogVlcMedia, Log, TEXT("FTimespan FVlcMediaPlayer::GetDuration() const"));
    return Duration;
}

TRange<float> FVlcMediaPlayer::GetSupportedRates(EMediaPlaybackDirections Direction, bool Unthinned) const
{
    UE_LOG(LogVlcMedia, Log, TEXT("TRange<float> FVlcMediaPlayer::GetSupportedRates(EMediaPlaybackDirections Direction, bool Unthinned) const"));
	//Doesn't support reverse :/
	if(Direction == EMediaPlaybackDirections::Reverse)
		return TRange<float>(0.0f, 0.0f);

	return TRange<float>(0.0f, 5.0f);
}

FString FVlcMediaPlayer::GetUrl() const
{
    UE_LOG(LogVlcMedia, Log, TEXT("FString FVlcMediaPlayer::GetUrl() const"));
    return MediaUrl;
}

bool FVlcMediaPlayer::SupportsRate(float Rate, bool Unthinned) const
{
    UE_LOG(LogVlcMedia, Log, TEXT("bool FVlcMediaPlayer::SupportsRate(float Rate, bool Unthinned) const"));
	//Doesn't support reverse :/
	if(Rate < 0)
		return false;

	return true;
}

bool FVlcMediaPlayer::SupportsScrubbing() const
{
    UE_LOG(LogVlcMedia, Log, TEXT("bool FVlcMediaPlayer::SupportsScrubbing() const"));
    //FIXME: Implement this
    return true;
}

bool FVlcMediaPlayer::SupportsSeeking() const
{
    UE_LOG(LogVlcMedia, Log, TEXT("bool FVlcMediaPlayer::SupportsSeeking() const"));
    if(vlc_player == NULL)
        return false;
        
    return libvlc_media_player_is_seekable(vlc_player);
}

/* IMediaPlayer interface
 *****************************************************************************/

void FVlcMediaPlayer::Close()
{
    UE_LOG(LogVlcMedia, Log, TEXT("void FVlcMediaPlayer::Close()"));
    
	if(vlc_player == NULL)
        return;
        
	UE_LOG(LogVlcMedia, Log, TEXT("WAHAAA"));
    
	//Release VLC elements
    if(vlc_player != NULL)
	{
		//if(IsPlaying())
		//	SetRate(0.0f);
		libvlc_media_player_stop(vlc_player);

		//while(libvlc_media_player_get_state(vlc_player) != libvlc_Playing)
		//	;
		libvlc_video_set_callbacks(vlc_player, NULL, NULL, NULL, NULL);
		libvlc_audio_set_callbacks(vlc_player, NULL, NULL, NULL, NULL, NULL, NULL);
        libvlc_media_player_release(vlc_player);
		vlc_player = NULL;
	}

	UE_LOG(LogVlcMedia, Log, TEXT("WAHAAA2"));

	//Release archive
	Archive.Reset();

	//Release Tracks
		ATracks.Reset();
		VTracks.Reset();
		CTracks.Reset();
		AudioTracks.Reset();
		VideoTracks.Reset();
		CaptionTracks.Reset();

	//Release buffer
	FrameBuffer.Reset();

	UE_LOG(LogVlcMedia, Log, TEXT("WAHAAA3"));
    
    //Release variables
    Duration = 0;
    MediaUrl = FString();
    Loop = false;

	UE_LOG(LogVlcMedia, Log, TEXT("WAHAAA4"));
    
    //Send events
    TracksChangedEvent.Broadcast();
    ClosedEvent.Broadcast();

	UE_LOG(LogVlcMedia, Log, TEXT("WAHAAA5"));
}

const TArray<IMediaAudioTrackRef>& FVlcMediaPlayer::GetAudioTracks() const
{   
    UE_LOG(LogVlcMedia, Log, TEXT("const TArray<IMediaAudioTrackRef>& FVlcMediaPlayer::GetAudioTracks() const"));

    return ATracks;
}

const TArray<IMediaCaptionTrackRef>& FVlcMediaPlayer::GetCaptionTracks() const
{   
    UE_LOG(LogVlcMedia, Log, TEXT("const TArray<IMediaCaptionTrackRef>& FVlcMediaPlayer::GetCaptionTracks() const"));

    return CTracks;
}

const IMediaInfo& FVlcMediaPlayer::GetMediaInfo() const
{
    UE_LOG(LogVlcMedia, Log, TEXT("const IMediaInfo& FVlcMediaPlayer::GetMediaInfo() const"));
    return *this;
}

float FVlcMediaPlayer::GetRate() const
{
    UE_LOG(LogVlcMedia, Log, TEXT("float FVlcMediaPlayer::GetRate() const"));
    
    if(vlc_player == NULL)
        return 0.0f;

	if(libvlc_media_player_get_state(vlc_player) != libvlc_Playing)
		return 0.0f;
        
    return libvlc_media_player_get_rate(vlc_player);;
}

FTimespan FVlcMediaPlayer::GetTime() const
{
    UE_LOG(LogVlcMedia, Log, TEXT("FTimespan FVlcMediaPlayer::GetTime() const"));
    if(vlc_player == NULL)
        return FTimespan::Zero();
        
	return FTimespan::FromMilliseconds(double(libvlc_media_player_get_time(vlc_player)));
}

const TArray<IMediaVideoTrackRef>& FVlcMediaPlayer::GetVideoTracks() const
{   
    UE_LOG(LogVlcMedia, Log, TEXT("const TArray<IMediaVideoTrackRef>& FVlcMediaPlayer::GetVideoTracks() const"));

    return VTracks;
}

bool FVlcMediaPlayer::IsLooping() const
{
    UE_LOG(LogVlcMedia, Log, TEXT("bool FVlcMediaPlayer::IsLooping() const"));
    return Loop;
}

bool FVlcMediaPlayer::IsPaused() const
{
    UE_LOG(LogVlcMedia, Log, TEXT("bool FVlcMediaPlayer::IsPaused() const"));
    if(vlc_player == NULL)
        return false;
        
    return libvlc_media_player_get_state(vlc_player) == libvlc_Paused;
}

bool FVlcMediaPlayer::IsPlaying() const
{
    UE_LOG(LogVlcMedia, Log, TEXT("bool FVlcMediaPlayer::IsPlaying() const"));
    if(vlc_player == NULL)
        return false;
        
    return libvlc_media_player_get_state(vlc_player) == libvlc_Playing;
}

bool FVlcMediaPlayer::IsReady() const
{
    UE_LOG(LogVlcMedia, Log, TEXT("bool FVlcMediaPlayer::IsReady() const"));
    if(vlc_player == NULL)
        return false;
        
	return libvlc_media_player_will_play(vlc_player) == 0 ? false : true;
}

bool FVlcMediaPlayer::Open(const FString& Url)
{   
    UE_LOG(LogVlcMedia, Log, TEXT("FVlcMediaPlayer::Open(const FString& Url)"));
    
    if(Url.IsEmpty()) {
        return false;
    }
    
    //Create media
	libvlc_media_t *vlc_media = libvlc_media_new_path(vlc_instance, TCHAR_TO_ANSI(*Url));
    
    if(vlc_media == NULL) {
        UE_LOG(LogVlcMedia, Log, TEXT("Failed to create media component"));
        return false;
    }
    
    UE_LOG(LogVlcMedia, Log, TEXT("Opening media..."));
    
    MediaUrl = Url;
    
    if(!Open(vlc_media))
    {
        MediaUrl = FString();
        return false;
    }
    
    return true;
}

bool FVlcMediaPlayer::Open(const TSharedRef<FArchive, ESPMode::ThreadSafe>& InArchive, const FString& OriginalUrl)
{   
    UE_LOG(LogVlcMedia, Log, TEXT("bool FVlcMediaPlayer::Open(const TSharedRef<FArchive, ESPMode::ThreadSafe>& InArchive, const FString& OriginalUrl)"));
    if((InArchive->TotalSize() == 0) || OriginalUrl.IsEmpty()) {
        return false;
    }
    
    Archive = InArchive;
    
    //Create media
	libvlc_media_t *vlc_media = libvlc_media_new_callbacks(vlc_instance, NULL, &FVlcMediaPlayer::BufferRead, &FVlcMediaPlayer::BufferSeek, &FVlcMediaPlayer::BufferClose, this);
    
    if(vlc_media == NULL) {
        UE_LOG(LogVlcMedia, Log, TEXT("Failed to create media component"));
        return false;
    }
    
    MediaUrl = OriginalUrl;
    
    if(!Open(vlc_media))
    {
        MediaUrl = FString();
        return false;
    }
    
    return true;
}

bool FVlcMediaPlayer::Seek(const FTimespan& Time)
{
    UE_LOG(LogVlcMedia, Log, TEXT("bool FVlcMediaPlayer::Seek(const FTimespan& Time)"));
    if(!SupportsSeeking())
        return false;
    
	libvlc_media_player_set_time(vlc_player, Time.GetTotalMilliseconds());
    
    return true;
}

bool FVlcMediaPlayer::SetLooping(bool Looping)
{
    UE_LOG(LogVlcMedia, Log, TEXT("bool FVlcMediaPlayer::SetLooping(bool Looping)"));
    if(vlc_player == NULL)
        return false;
        
    this->Loop = Looping;
    
    return true;
}

bool FVlcMediaPlayer::SetRate(float Rate)
{
    UE_LOG(LogVlcMedia, Log, TEXT("bool FVlcMediaPlayer::SetRate(float Rate)"));
    if(vlc_player == NULL)
        return false;
        
	if(libvlc_media_player_set_rate(vlc_player, Rate) != 0)
		return false;
    
    //Play/pause according the rate
    if(Rate == 0.0f)
    {
        libvlc_media_player_set_pause(vlc_player, 1);
        return true;
    }
	else
		return libvlc_media_player_play(vlc_player) == 0 ? true : false;
}

/* FVlcVideoPlayer members
*****************************************************************************/

bool FVlcMediaPlayer::ChangeStreamState(EMediaStreamType StreamType, int32 ID, bool Enabled)
{
    UE_LOG(LogVlcMedia, Log, TEXT("bool FVlcMediaPlayer::ChangeStreamState(EMediaStreamType StreamType, int32 ID, bool Enabled)"));
    switch(StreamType)
    {
        case EMediaStreamType::MEDIA_STREAM_AUDIO:
			UE_LOG(LogVlcMedia, Log, TEXT("ChangeStreamState <Audio, %i, %i>"), ID, Enabled);

			if(!AudioTracks.Contains(ID))
				return false;

            if(!Enabled)
            {
                if(CurrentAudioTrack.IsValid() && StaticCastSharedPtr<FVlcMediaAudioTrack>(CurrentAudioTrack)->GetID() == ID)
                    CurrentAudioTrack.Reset();
                    
                StaticCastSharedRef<FVlcMediaAudioTrack>(AudioTracks[ID])->ChangeEnabledState(false);
                
                return true;
            }
            
            if(libvlc_audio_set_track(vlc_player, ID) == 0)
            {
                for (auto& Elem : AudioTracks)
                {
                    if(Elem.Key == ID)
                        StaticCastSharedRef<FVlcMediaAudioTrack>(Elem.Value)->ChangeEnabledState(true);
                    else
                        StaticCastSharedRef<FVlcMediaAudioTrack>(Elem.Value)->ChangeEnabledState(false);
                }
                
                CurrentAudioTrack = AudioTracks[ID];
            }
            else
                return false;

			//Set format according to track
			libvlc_audio_set_format(vlc_player, "S16L", AudioTracks[ID]->GetSamplesPerSecond(), AudioTracks[ID]->GetNumChannels());
            
            TracksChangedEvent.Broadcast();
                    
            return true;
        case EMediaStreamType::MEDIA_STREAM_VIDEO:
			UE_LOG(LogVlcMedia, Log, TEXT("ChangeStreamState <Video, %i, %i>"), ID, Enabled);

			if(!VideoTracks.Contains(ID))
				return false;

            if(!Enabled)
            {
                if(CurrentVideoTrack.IsValid() && StaticCastSharedPtr<FVlcMediaVideoTrack>(CurrentVideoTrack)->GetID() == ID)
                    CurrentVideoTrack.Reset();
                    
                StaticCastSharedRef<FVlcMediaVideoTrack>(VideoTracks[ID])->ChangeEnabledState(false);
                
                return true;
            }
            
            if(libvlc_video_set_track(vlc_player, ID) == 0)
            {
                for (auto& Elem : VideoTracks)
                {
                    if(Elem.Key == ID)
                        StaticCastSharedRef<FVlcMediaVideoTrack>(Elem.Value)->ChangeEnabledState(true);
                    else
                        StaticCastSharedRef<FVlcMediaVideoTrack>(Elem.Value)->ChangeEnabledState(false);
                }
                
                CurrentVideoTrack = VideoTracks[ID];
            }
            else
                return false;

			//Set format according to track
			libvlc_video_set_format(vlc_player, "RV32", VideoTracks[ID]->GetDimensions().X, VideoTracks[ID]->GetDimensions().Y, VideoTracks[ID]->GetDimensions().X * 4);
			FrameBuffer.SetNumUninitialized(VideoTracks[ID]->GetDimensions().X * VideoTracks[ID]->GetDimensions().Y * 4);

			TracksChangedEvent.Broadcast();
                
            return true;
        case EMediaStreamType::MEDIA_STREAM_CAPTION:
			UE_LOG(LogVlcMedia, Log, TEXT("ChangeStreamState <Video, %i, %i>"), ID, Enabled);
			if(!CaptionTracks.Contains(ID))
				return false;

            if(!Enabled)
            {
                if(CurrentCaptionTrack.IsValid() && StaticCastSharedPtr<FVlcMediaCaptionTrack>(CurrentCaptionTrack)->GetID() == ID)
                    CurrentCaptionTrack.Reset();
                    
                StaticCastSharedRef<FVlcMediaCaptionTrack>(CaptionTracks[ID])->ChangeEnabledState(false);
                
                return true;
            }
            
            if(libvlc_video_set_spu(vlc_player, ID) == 0)
            {
                for (auto& Elem : CaptionTracks)
                {
                    if(Elem.Key == ID)
                        StaticCastSharedRef<FVlcMediaCaptionTrack>(Elem.Value)->ChangeEnabledState(true);
                    else
                        StaticCastSharedRef<FVlcMediaCaptionTrack>(Elem.Value)->ChangeEnabledState(false);
                }
                
                CurrentCaptionTrack = CaptionTracks[ID];
            }
            else
                return false;
            
            TracksChangedEvent.Broadcast();
                
            return true;
        default:
            return false;
    }
}

IMediaAudioTrackPtr& FVlcMediaPlayer::GetCurrentAudioTrack() 
{
    UE_LOG(LogVlcMedia, Log, TEXT("IMediaAudioTrackPtr& FVlcMediaPlayer::GetCurrentAudioTrack()"));
    
    return CurrentAudioTrack;
}

IMediaVideoTrackPtr& FVlcMediaPlayer::GetCurrentVideoTrack()
{
    UE_LOG(LogVlcMedia, Log, TEXT("IMediaVideoTrackPtr& FVlcMediaPlayer::GetCurrentVideoTrack()"));
    
    return CurrentVideoTrack;
}

IMediaCaptionTrackPtr& FVlcMediaPlayer::GetCurrentCaptionTrack()
{
    UE_LOG(LogVlcMedia, Log, TEXT("IMediaCaptionTrackPtr& FVlcMediaPlayer::GetCurrentCaptionTrack()"));
    
    return CurrentCaptionTrack;
}

TSharedPtr<FArchive, ESPMode::ThreadSafe>& FVlcMediaPlayer::GetArchive()
{
    UE_LOG(LogVlcMedia, Log, TEXT("TSharedPtr<FArchive, ESPMode::ThreadSafe>& FVlcMediaPlayer::GetArchive()"));
    
    return Archive;
}

TArray<uint8>& FVlcMediaPlayer::GetFrameBuffer()
{
    UE_LOG(LogVlcMedia, Log, TEXT("TArray<uint8>& FVlcMediaPlayer::GetFrameBuffer()"));
    
    return FrameBuffer;
}

bool FVlcMediaPlayer::Open(libvlc_media_t *Media)
{
	UE_LOG(LogVlcMedia, Log, TEXT("bool FVlcMediaPlayer::Open(libvlc_media_t *Media)"));

	if(Media == NULL)
		return false;
    
    //Create player
    vlc_player = libvlc_media_player_new_from_media(Media);

	libvlc_media_release(Media);
    
    if(vlc_player == NULL) {
        UE_LOG(LogVlcMedia, Log, TEXT("Failed to create media player"));
        return false;
    }
    
	UE_LOG(LogVlcMedia, Log, TEXT("Created Media Player..."));
    
    //Set callbacks
    libvlc_video_set_callbacks(vlc_player, &FVlcMediaPlayer::VideoLock, NULL, &FVlcMediaPlayer::VideoDisplay, this);
    libvlc_audio_set_callbacks(vlc_player, &FVlcMediaPlayer::AudioPlay, NULL, NULL, NULL, NULL, this);
	libvlc_event_attach(libvlc_media_player_event_manager(vlc_player), libvlc_MediaPlayerEndReached , &FVlcMediaPlayer::PlayerEndReached, this);

	//Hack: do this to obtain player/media data
	libvlc_media_parse(libvlc_media_player_get_media(vlc_player));
	libvlc_media_player_play(vlc_player);
	while(libvlc_media_player_get_state(vlc_player) != libvlc_Playing);
	libvlc_media_player_set_pause(vlc_player, 1);
	while(libvlc_media_player_get_state(vlc_player) != libvlc_Paused);

	//Sets duration of the media
	Duration = FTimespan::FromMilliseconds(libvlc_media_get_duration(libvlc_media_player_get_media(vlc_player)) >= 0 ? double(libvlc_media_get_duration(libvlc_media_player_get_media(vlc_player))) : 0);
    
    //Get information about the media
	//==========================================

	//Tracks
	libvlc_media_track_t **m_tracks;
	unsigned int n_tracks = libvlc_media_tracks_get(libvlc_media_player_get_media(vlc_player), &m_tracks);

	if(n_tracks == 0)
	{
		UE_LOG(LogVlcMedia, Error, TEXT("Media file has no tracks"));
		libvlc_media_player_release(vlc_player);
		return false;
	}

	for (int i = 0; i < n_tracks; i++)
	{
		switch (m_tracks[i]->i_type)
		{
			case libvlc_track_audio:
				AudioTracks.Add(m_tracks[i]->i_id, MakeShareable(new FVlcMediaAudioTrack(*this, m_tracks[i]->i_id, m_tracks[i]->psz_description != NULL ? FString(ANSI_TO_TCHAR(m_tracks[i]->psz_description)) :  FString::Printf(ANSI_TO_TCHAR("Track %i"), m_tracks[i]->i_id), FString(ANSI_TO_TCHAR(m_tracks[i]->psz_language)), m_tracks[i]->audio->i_channels, m_tracks[i]->audio->i_rate)));
				break;
			case libvlc_track_video:
				VideoTracks.Add(m_tracks[i]->i_id, MakeShareable(new FVlcMediaVideoTrack(*this, m_tracks[i]->i_id, m_tracks[i]->psz_description != NULL ? FString(ANSI_TO_TCHAR(m_tracks[i]->psz_description)) :  FString::Printf(ANSI_TO_TCHAR("Track %i"), m_tracks[i]->i_id), FString(ANSI_TO_TCHAR(m_tracks[i]->psz_language)), m_tracks[i]->video->i_width, m_tracks[i]->video->i_height, m_tracks[i]->i_bitrate, m_tracks[i]->video->i_frame_rate_num)));
				break;
			case libvlc_track_text:
				CaptionTracks.Add(m_tracks[i]->i_id, MakeShareable(new FVlcMediaCaptionTrack(*this, m_tracks[i]->i_id, m_tracks[i]->psz_description != NULL ? FString(ANSI_TO_TCHAR(m_tracks[i]->psz_description)) :  FString::Printf(ANSI_TO_TCHAR("Track %i"), m_tracks[i]->i_id), FString(ANSI_TO_TCHAR(m_tracks[i]->psz_language)))));
				break;
		}
	}

	libvlc_media_tracks_release(m_tracks, n_tracks);

	//Generate tracks array
	AudioTracks.GenerateValueArray(ATracks);
	VideoTracks.GenerateValueArray(VTracks);
	CaptionTracks.GenerateValueArray(CTracks);

	//Enable tracks according to language
	UE_LOG(LogVlcMedia, Log, TEXT("Language: %s"), *FInternationalization::Get().GetCurrentCulture()->GetName());

	FString CurrentLanguage = FInternationalization::Get().GetCurrentCulture()->GetName();

	for(auto& Track : ATracks)
	{
		if(Track->GetStream().GetLanguage() == CurrentLanguage)
		{
			static_cast<FVlcMediaStream&>(Track->GetStream()).Enable();
			break;
		}
	}

	for(auto& Track : VTracks)
	{
		if(Track->GetStream().GetLanguage() == CurrentLanguage)
		{
			static_cast<FVlcMediaStream&>(Track->GetStream()).Enable();
			break;
		}
	}

	for(auto& Track : CTracks)
	{
		if(Track->GetStream().GetLanguage() == CurrentLanguage)
		{
			static_cast<FVlcMediaStream&>(Track->GetStream()).Enable();
			break;
		}
	}

	UE_LOG(LogVlcMedia, Log, TEXT("E"));

	//Set fallback tracks if not found
	if(!CurrentAudioTrack.IsValid() && ATracks.Num() > 0)
		static_cast<FVlcMediaStream&>(ATracks[0]->GetStream()).Enable();
		//StaticCastSharedRef<FVlcMediaStream>(ATracks[0])->GetStream().Enable();

	UE_LOG(LogVlcMedia, Log, TEXT("E1"));

	if(!CurrentVideoTrack.IsValid() && VTracks.Num() > 0)
		//StaticCastSharedRef<FVlcMediaVideoTrack>(VTracks[0])->GetStream().Enable();
		static_cast<FVlcMediaStream&>(VTracks[0]->GetStream()).Enable();

	UE_LOG(LogVlcMedia, Log, TEXT("E2"));

	if(!CurrentCaptionTrack.IsValid() && CTracks.Num() > 0)
		//StaticCastSharedRef<FVlcMediaCaptionTrack>(CTracks[0])->GetStream().Enable();
		static_cast<FVlcMediaStream&>(CTracks[0]->GetStream()).Enable();
    
	UE_LOG(LogVlcMedia, Log, TEXT("E3"));

    //Send event
    OpenedEvent.Broadcast(MediaUrl);
	TracksChangedEvent.Broadcast();
    
    UE_LOG(LogVlcMedia, Log, TEXT("F"));
    
    return true;
}

void FVlcMediaPlayer::AudioPlay(void *Opaque, const void *Samples, unsigned Count, int64_t Pts)
{
    UE_LOG(LogVlcMedia, Log, TEXT("void FVlcMediaPlayer::AudioPlay(void *Opaque, const void *Samples, unsigned Count, int64_t Pts)"));
    
    FVlcMediaPlayer *player = static_cast<FVlcMediaPlayer *>(Opaque);
    
	if(!player->GetCurrentAudioTrack().IsValid() || player->GetFrameBuffer().Num() == 0)
		return;
        
    FVlcMediaStream& MediaStream = static_cast<FVlcMediaStream&>(player->GetCurrentAudioTrack()->GetStream());

	MediaStream.ProcessMedia((void *)Samples, Count * 2, FTimespan::FromMilliseconds(1000.0 * double(Count)), player->GetTime());
}

void *FVlcMediaPlayer::VideoLock(void *Opaque, void **Planes)
{
    UE_LOG(LogVlcMedia, Log, TEXT("void *FVlcMediaPlayer::VideoLock(void *Opaque, void **Planes)"));
    
    FVlcMediaPlayer *player = static_cast<FVlcMediaPlayer *>(Opaque);
    
	if(player->GetFrameBuffer().Num() > 0)
		*Planes = player->GetFrameBuffer().GetData();
    
    return NULL;
}

void FVlcMediaPlayer::VideoDisplay(void *Opaque, void *Picture)
{
    UE_LOG(LogVlcMedia, Log, TEXT("void FVlcMediaPlayer::VideoDisplay(void *Opaque, void *Picture)"));
    
    FVlcMediaPlayer *player = static_cast<FVlcMediaPlayer *>(Opaque);
    
	if(!player->GetCurrentVideoTrack().IsValid() || player->GetFrameBuffer().Num() == 0)
		return;

    FVlcMediaStream& MediaStream = static_cast<FVlcMediaStream&>(player->GetCurrentVideoTrack()->GetStream());

	MediaStream.ProcessMedia((void *)player->GetFrameBuffer().GetData(), player->GetFrameBuffer().Num(), FTimespan::FromMilliseconds(1000.0 / double(player->GetCurrentVideoTrack()->GetFrameRate())), player->GetTime());
}

void FVlcMediaPlayer::PlayerEndReached(const struct libvlc_event_t *Event, void *Opaque)
{
	UE_LOG(LogVlcMedia, Log, TEXT("void FVlcMediaPlayer::PlayerEndReached(const struct libvlc_event_t *Event, void *Opaque)"));

	FVlcMediaPlayer *player = static_cast<FVlcMediaPlayer *>(Opaque);

	if(player->IsLooping())
	{
		player->Seek(FTimespan(0));
		player->SetRate(1.0);
	}
}

ssize_t FVlcMediaPlayer::BufferRead(void *Opaque, unsigned char *Buffer, size_t Length)
{    
    UE_LOG(LogVlcMedia, Log, TEXT("ssize_t FVlcMediaPlayer::BufferRead(void *Opaque, unsigned char *Buffer, size_t Length)"));
    
    FVlcMediaPlayer *player = static_cast<FVlcMediaPlayer *>(Opaque);
    
    if(!player->GetArchive().IsValid())
        return 0;
    
    if(player->GetArchive()->AtEnd())
        return 0;
    
    int64 to_read = Length;
    
    if(Length + player->GetArchive()->Tell() > player->GetArchive()->TotalSize())
        to_read = player->GetArchive()->TotalSize() - player->GetArchive()->Tell();
        
    if(to_read > 0)
        player->GetArchive()->Serialize(Buffer, to_read);
        
    return (ssize_t)to_read;
}

int FVlcMediaPlayer::BufferSeek(void *Opaque, uint64_t Offset)
{       
    UE_LOG(LogVlcMedia, Log, TEXT("int FVlcMediaPlayer::BufferSeek(void *Opaque, uint64_t Offset)"));
    
    FVlcMediaPlayer *player = static_cast<FVlcMediaPlayer *>(Opaque);
    
    if(!player->GetArchive().IsValid())
        return -1;
        
    if(player->GetArchive()->AtEnd())
        return -1;
    
    if(Offset < player->GetArchive()->TotalSize())
        player->GetArchive()->Seek(Offset);
    else
        return -1;
    
    return 0;
}

void FVlcMediaPlayer::BufferClose(void *Opaque)
{
    UE_LOG(LogVlcMedia, Log, TEXT("void FVlcMediaPlayer::BufferClose(void *Opaque)"));
    
    FVlcMediaPlayer *player = static_cast<FVlcMediaPlayer *>(Opaque);
    
    if (!player->GetArchive().IsValid())
        return;
        
    player->GetArchive()->Seek(0);
}
