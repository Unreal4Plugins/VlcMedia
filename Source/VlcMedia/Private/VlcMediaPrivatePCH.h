#pragma once

#include "Core.h"

//#include "Runtime/Engine/Public/Engine.h"
#include "Runtime/Media/Public/IMediaInfo.h"
#include "Runtime/Media/Public/IMediaPlayer.h"
#include "Runtime/Media/Public/IMediaSink.h"
#include "Runtime/Media/Public/IMediaStream.h"
#include "Runtime/Media/Public/IMediaAudioTrack.h"
#include "Runtime/Media/Public/IMediaCaptionTrack.h"
#include "Runtime/Media/Public/IMediaVideoTrack.h"
#include "Runtime/Core/Public/Templates/SharedPointer.h"
#include "Runtime/Core/Public/Serialization/ArchiveBase.h"
#include "Runtime/Core/Public/Internationalization/Internationalization.h"

#include <dlfcn.h>
#include <vlc/vlc.h>

DECLARE_LOG_CATEGORY_EXTERN(LogVlcMedia, Log, All);

#include "VlcISO639.h"
#include "Enumerations.h"
#include "VlcMediaPlayer.h"
#include "VlcMediaStream.h"
#include "VlcMediaAudioTrack.h"
#include "VlcMediaVideoTrack.h"
#include "VlcMediaCaptionTrack.h"

