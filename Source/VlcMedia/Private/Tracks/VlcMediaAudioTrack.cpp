#include "VlcMediaPrivatePCH.h"

/* FVlcMediaAudioTrack constructors
*****************************************************************************/

FVlcMediaAudioTrack::FVlcMediaAudioTrack(FVlcMediaPlayer& TrackMediaPlayer, int32 TrackID, FString TrackName, FString TrackLanguage, uint32 TrackNumChannels, uint32 TrackSamplesPerSecond):
	FVlcMediaStream(TrackMediaPlayer, TrackID, TrackName, TrackLanguage, EMediaStreamType::MEDIA_STREAM_AUDIO)
    , MediaPlayer(TrackMediaPlayer)
	, ID(TrackID)
    , NumChannels(TrackNumChannels)
    , SamplesPerSecond(TrackSamplesPerSecond)
{
}

FVlcMediaAudioTrack::~FVlcMediaAudioTrack()
{
    //Release variables
    NumChannels = 0;
    SamplesPerSecond = 0;
	ID = -1;
}

/* IMediaAudioTrack interface
 *****************************************************************************/

uint32 FVlcMediaAudioTrack::GetNumChannels() const
{
    return NumChannels;
}

uint32 FVlcMediaAudioTrack::GetSamplesPerSecond() const
{
    return SamplesPerSecond;
}

IMediaStream& FVlcMediaAudioTrack::GetStream()
{
    return *this;
}

/* FVlcMediaAudioTrack implementation
 *****************************************************************************/
 
int32 FVlcMediaAudioTrack::GetID()
{
    return ID;
}
