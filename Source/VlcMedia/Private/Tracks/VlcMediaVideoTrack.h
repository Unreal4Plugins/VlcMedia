class FVlcMediaVideoTrack : public IMediaVideoTrack, public FVlcMediaStream
{
public:
	FVlcMediaVideoTrack(FVlcMediaPlayer& TrackMediaPlayer,
                        int32 TrackID,
                        FString TrackName,
						FString TrackLanguage,
                        uint32 TrackWidth,
                        uint32 TrackHeight,
                        uint32 TrackBitRate,
                        float TrackFrameRate);
    ~FVlcMediaVideoTrack();

    // IMediaVideoTrack interface
    virtual uint32 GetBitRate() const override;
    virtual FIntPoint GetDimensions() const override;
    virtual float GetFrameRate() const override;
    virtual IMediaStream& GetStream() override;

    // FVlcMediaVideoTrack implementation
	int32 GetID();

private:
	FVlcMediaPlayer& MediaPlayer;
    
	int32 ID;
    uint32 Width;
    uint32 Height;
    uint32 BitRate;
    float FrameRate;
};

typedef TSharedPtr<FVlcMediaVideoTrack, ESPMode::ThreadSafe> FVlcMediaVideoTrackPtr;
typedef TSharedRef<FVlcMediaVideoTrack, ESPMode::ThreadSafe> FVlcMediaVideoTrackRef;
