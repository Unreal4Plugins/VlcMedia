#include "VlcMediaPrivatePCH.h"

/* FVlcMediaVideoTrack constructors
*****************************************************************************/

FVlcMediaVideoTrack::FVlcMediaVideoTrack(FVlcMediaPlayer& TrackMediaPlayer,
                                         int32 TrackID,
                                         FString TrackName,
										 FString TrackLanguage,
                                         uint32 TrackWidth,
                                         uint32 TrackHeight,
                                         uint32 TrackBitRate,
                                         float TrackFrameRate)
	: FVlcMediaStream(TrackMediaPlayer, TrackID, TrackName, TrackLanguage, EMediaStreamType::MEDIA_STREAM_VIDEO)
    , MediaPlayer(TrackMediaPlayer)
	, ID(TrackID)
    , Width(TrackWidth)
    , Height(TrackHeight)
    , BitRate(TrackBitRate)
    , FrameRate(TrackFrameRate)
    
{
}

FVlcMediaVideoTrack::~FVlcMediaVideoTrack()
{
    // Release variables
    BitRate = 0;
    FrameRate = 0;
    Width = 0;
    Height = 0;
	ID = -1;
}

/* IMediaVideoTrack interface
 *****************************************************************************/

uint32 FVlcMediaVideoTrack::GetBitRate() const
{
    return BitRate;
}

FIntPoint FVlcMediaVideoTrack::GetDimensions() const
{
    return FIntPoint(Width, Height);
}

float FVlcMediaVideoTrack::GetFrameRate() const
{
    return FrameRate;
}

IMediaStream& FVlcMediaVideoTrack::GetStream()
{
    return *this;
}

/* FVlcMediaVideoTrack implementation
 *****************************************************************************/
 
int32 FVlcMediaVideoTrack::GetID()
{
    return ID;
}
