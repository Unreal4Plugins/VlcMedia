class FVlcMediaStream : public IMediaStream
{
public:
	FVlcMediaStream(FVlcMediaPlayer& StreamMediaPlayer, int32 StreamID, FString StreamName, FString StreamLanguage, EMediaStreamType StreamType);
    ~FVlcMediaStream();

    // IMediaStream interface
    virtual void AddSink(const IMediaSinkRef& Sink) override;
    virtual bool Disable() override;
    virtual bool Enable() override;
    virtual FText GetDisplayName() const override;
    virtual FString GetLanguage() const override;
    virtual FString GetName() const override;
    virtual bool IsEnabled() const override;
    virtual bool IsMutuallyExclusive(const IMediaStreamRef& Other) const override;
    virtual bool IsProtected() const override;
    virtual void RemoveSink(const IMediaSinkRef& Sink) override;

    // FVlcMediaStream implementation
    void ProcessMedia(void* SampleBuffer,
                      uint32 SampleSize,
                      const FTimespan& SampleDuration,
                      const FTimespan& SampleTime);
    EMediaStreamType GetType() const;
    void ChangeEnabledState(bool IsEnabled);

protected:
	FVlcMediaPlayer& MediaPlayer;
    int32 SID;
    
	FString Name;
    FString Language;
    EMediaStreamType MediaStreamType;   
    bool Enabled;
    TArray<IMediaSinkWeakPtr> Sinks;
    
};

typedef TSharedPtr<FVlcMediaStream, ESPMode::ThreadSafe> FVlcMediaStreamPtr;
typedef TSharedRef<FVlcMediaStream, ESPMode::ThreadSafe> FVlcMediaStreamRef;
