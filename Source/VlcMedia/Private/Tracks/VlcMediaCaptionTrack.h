class FVlcMediaCaptionTrack : public IMediaCaptionTrack, public FVlcMediaStream
{
public:
	FVlcMediaCaptionTrack(FVlcMediaPlayer& TrackMediaPlayer, int32 TrackID, FString TrackName, FString TrackLanguage);
    ~FVlcMediaCaptionTrack();

    // IMediaCaptionTrack interface
    virtual IMediaStream& GetStream() override;

    // FVlcMediaCaptionTrack implementation
	int32 GetID();

private:
	FVlcMediaPlayer& MediaPlayer;
    
	int32 ID;
};

typedef TSharedPtr<FVlcMediaCaptionTrack, ESPMode::ThreadSafe> FVlcMediaCaptionTrackPtr;
typedef TSharedRef<FVlcMediaCaptionTrack, ESPMode::ThreadSafe> FVlcMediaCaptionTrackRef;
