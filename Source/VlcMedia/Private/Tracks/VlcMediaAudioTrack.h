class FVlcMediaAudioTrack : public IMediaAudioTrack, public FVlcMediaStream
{
public:
	FVlcMediaAudioTrack(FVlcMediaPlayer& TrackMediaPlayer,
                        int32 TrackID,
                        FString TrackName,
						FString TrackLanguage,
                        uint32 TrackNumChannels,
                        uint32 TrackSamplesPerSecond);
    ~FVlcMediaAudioTrack();

    // IMediaAudioTrack interface
    virtual uint32 GetNumChannels() const override;
    virtual uint32 GetSamplesPerSecond() const override;
    virtual IMediaStream& GetStream() override;

    // FVlcMediaAudioTrack implementation
	int32 GetID();

private:
	FVlcMediaPlayer& MediaPlayer;
    
	int32 ID;
    uint32 NumChannels;
    uint32 SamplesPerSecond;
};

typedef TSharedPtr<FVlcMediaAudioTrack, ESPMode::ThreadSafe> FVlcMediaAudioTrackPtr;
typedef TSharedRef<FVlcMediaAudioTrack, ESPMode::ThreadSafe> FVlcMediaAudioTrackRef;
