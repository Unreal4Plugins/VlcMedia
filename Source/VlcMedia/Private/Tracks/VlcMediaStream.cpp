#include "VlcMediaPrivatePCH.h"

/* FVlcMediaStream constructors
*****************************************************************************/

FVlcMediaStream::FVlcMediaStream(FVlcMediaPlayer& StreamMediaPlayer, int32 StreamID, FString StreamName, FString StreamLanguage, EMediaStreamType StreamType)
    : MediaPlayer(StreamMediaPlayer)
    , SID(StreamID)
	, Name(StreamName)
    , Language(StreamLanguage)
    , MediaStreamType(StreamType)
    , Enabled(false)
{
}

FVlcMediaStream::~FVlcMediaStream()
{
    // Release pointers
    Sinks.Reset();

    // Release variables
    Language = FString();
	Name = FString();
    Enabled = false;

}

/* IMediaStream interface
 *****************************************************************************/

void FVlcMediaStream::AddSink(const IMediaSinkRef& Sink)
{
    Sinks.AddUnique(Sink);
}

bool FVlcMediaStream::Disable()
{
	bool retvalue = MediaPlayer.ChangeStreamState(MediaStreamType, SID, false);

    if(retvalue == true)
        Enabled = false;
        
    return retvalue;
}

bool FVlcMediaStream::Enable()
{
	bool retvalue = MediaPlayer.ChangeStreamState(MediaStreamType, SID, true);

    if(retvalue == true)
        Enabled = true;
        
    return retvalue;
}

FText FVlcMediaStream::GetDisplayName() const
{
	return FText::FromString(FString::Printf(ANSI_TO_TCHAR("%s [%s]"), *Name, ANSI_TO_TCHAR(FVlcISO639::GetLangDescription(TCHAR_TO_ANSI(*Language)))));
}

FString FVlcMediaStream::GetLanguage() const
{
	//Convert to ISO 639-1

	return FString(ANSI_TO_TCHAR(FVlcISO639::GetLang_1(TCHAR_TO_ANSI(*Language))));
}

FString FVlcMediaStream::GetName() const
{
	return Name;
}

bool FVlcMediaStream::IsEnabled() const
{
    return Enabled;
}

bool FVlcMediaStream::IsMutuallyExclusive(const IMediaStreamRef& Other) const
{
    FVlcMediaStreamRef VlcMediaStream = StaticCastSharedRef<FVlcMediaStream>(Other);
    
    
    return VlcMediaStream->GetType() == GetType();
}

bool FVlcMediaStream::IsProtected() const
{
    // FIXME: Implement DRM support
    return false;
}

void FVlcMediaStream::RemoveSink(const IMediaSinkRef& Sink)
{
    Sinks.Remove(Sink);
}

/* FVlcMediaStream members
*****************************************************************************/

void FVlcMediaStream::ProcessMedia(void* SampleBuffer,
                                  uint32 SampleSize,
                                  const FTimespan& SampleDuration,
                                  const FTimespan& SampleTime)
{
    for(IMediaSinkWeakPtr& SinkRef : Sinks) {

        IMediaSinkPtr Sink = SinkRef.Pin();

        if(Sink.IsValid()) {
            Sink->ProcessMediaSample(SampleBuffer, SampleSize, SampleDuration, SampleTime);
        }
    }
}

EMediaStreamType FVlcMediaStream::GetType() const
{
    return MediaStreamType;
}

void FVlcMediaStream::ChangeEnabledState(bool IsEnabled)
{
    Enabled = IsEnabled;
}
