#include "VlcMediaPrivatePCH.h"

/* FVlcMediaCaptionTrack constructors
*****************************************************************************/

FVlcMediaCaptionTrack::FVlcMediaCaptionTrack(FVlcMediaPlayer& TrackMediaPlayer, int32 TrackID, FString TrackName, FString TrackLanguage)
	: FVlcMediaStream(TrackMediaPlayer, TrackID, TrackName, TrackLanguage, EMediaStreamType::MEDIA_STREAM_CAPTION)
	, MediaPlayer(TrackMediaPlayer), ID(TrackID)
{
}

FVlcMediaCaptionTrack::~FVlcMediaCaptionTrack()
{
    // Release variables
	ID = -1;
}

/* IMediaCaptionTrack interface
 *****************************************************************************/

IMediaStream& FVlcMediaCaptionTrack::GetStream()
{
    return *this;
}

/* FVlcMediaCaptionTrack implementation
 *****************************************************************************/

int32 FVlcMediaCaptionTrack::GetID()
{
    return ID;
}

