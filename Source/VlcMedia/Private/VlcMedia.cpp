#include "VlcMediaPrivatePCH.h"
#include "VlcMedia.h"

DEFINE_LOG_CATEGORY(LogVlcMedia);

#define LOCTEXT_NAMESPACE "FVlcMediaModule"

void FVlcMediaModule::StartupModule()
{
    IMediaModule* MediaModule = FModuleManager::LoadModulePtr<IMediaModule>("Media");

    if(MediaModule == nullptr) {
        UE_LOG(LogVlcMedia, Log, TEXT("Failed to load Media module"));
        return;
    }

    if(!LoadRequiredLibraries()) {
        UE_LOG(LogVlcMedia, Log, TEXT("Failed to load required VLC libraries"));
        return;
    }

    // Initialize VLC

    // Get/create system and VLC arguments
    const char* vlc_argv[] = {
        "--no-xlib",
		"--vout=dummy",
		"--aout=dummy",
    };

    int vlc_argc = sizeof(vlc_argv) / sizeof(*vlc_argv);

    // Create a VLC instance
    vlc_instance = libvlc_new(vlc_argc, vlc_argv);

    if(vlc_instance == NULL) {
        UE_LOG(LogVlcMedia, Log, TEXT("Failed to initialize VLC"));
        return;
    }

    // Initialize supported media formats
    SupportedFileTypes.Add(TEXT("flv"), LOCTEXT("FormatFlv", "Flash Video File"));
    SupportedFileTypes.Add(TEXT("3gp"), LOCTEXT("Format3gp", "3GP Video Stream"));
    SupportedFileTypes.Add(TEXT("mkv"), LOCTEXT("FormatMkv", "Matroska Multimedia File"));
    SupportedFileTypes.Add(TEXT("mka"), LOCTEXT("FormatMka", "Matroska Multimedia File"));
    SupportedFileTypes.Add(TEXT("aac"), LOCTEXT("FormatAac", "MPEG-2 Advanced Audio Coding File"));
    SupportedFileTypes.Add(TEXT("au"), LOCTEXT("FormatAu", "Audio File"));
    SupportedFileTypes.Add(TEXT("wav"), LOCTEXT("FormatWav", "Audio File"));
    SupportedFileTypes.Add(TEXT("dts"), LOCTEXT("FormatDts", "Audio File"));
    SupportedFileTypes.Add(TEXT("asf"), LOCTEXT("FormatAsf", "ASF Media File"));
    SupportedFileTypes.Add(TEXT("avi"), LOCTEXT("FormatAvi", "Audio Video Interleave File"));
    SupportedFileTypes.Add(TEXT("nsc"), LOCTEXT("FormatNsc", "NSC Format"));
    SupportedFileTypes.Add(TEXT("nsv"), LOCTEXT("FormatNsv", "NSV Format"));
    SupportedFileTypes.Add(TEXT("nut"), LOCTEXT("FormatNut", "NUT Format"));
    SupportedFileTypes.Add(TEXT("mov"), LOCTEXT("FormatMov", "Apple QuickTime Movie"));
    SupportedFileTypes.Add(TEXT("ogm"), LOCTEXT("FormatOgm", "OGG Multimedia File"));
    SupportedFileTypes.Add(TEXT("ogg"), LOCTEXT("FormatOgg", "OGG Multimedia File"));
    SupportedFileTypes.Add(TEXT("mp3"), LOCTEXT("FormatMp3", "MPEG-2 Audio"));
    SupportedFileTypes.Add(TEXT("mp2"), LOCTEXT("FormatMp2", "MPEG-2 Audio"));
    SupportedFileTypes.Add(TEXT("mpg"), LOCTEXT("FormatMpg", "MPEG-2 Format"));
    SupportedFileTypes.Add(TEXT("ts"), LOCTEXT("FormatMpg", "MPEG-2 Format"));
    SupportedFileTypes.Add(TEXT("mp4"), LOCTEXT("FormatMp4", "MPEG-4 Movie"));
    SupportedFileTypes.Add(TEXT("rm"), LOCTEXT("FormatRm", "Real Media Format File"));
    SupportedFileTypes.Add(TEXT("ra"), LOCTEXT("FormatRa", "Real Media Format File"));
    SupportedFileTypes.Add(TEXT("ram"), LOCTEXT("FormatRam", "Real Media Format File"));
    SupportedFileTypes.Add(TEXT("rv"), LOCTEXT("FormatRv", "Real Media Format File"));
    SupportedFileTypes.Add(TEXT("rmvb"), LOCTEXT("FormatRmvb", "Real Media Format File"));
    SupportedFileTypes.Add(TEXT("a52"), LOCTEXT("FormatA52", "A52 Multimedia File"));
    SupportedFileTypes.Add(TEXT("wmv"), LOCTEXT("FormatWmv", "Windows Media Video File"));
    SupportedFileTypes.Add(TEXT("dts"), LOCTEXT("FormatDts", "DTS Multimedia File"));
    SupportedFileTypes.Add(TEXT("flac"), LOCTEXT("FormatFlac", "Free-loseless audio File"));
    SupportedFileTypes.Add(TEXT("dv"), LOCTEXT("FormatDv", "Digital video Multimedia File"));
    SupportedFileTypes.Add(TEXT("vid"), LOCTEXT("FormatVid", "VID Multimedia File"));
    SupportedFileTypes.Add(TEXT("dts"), LOCTEXT("FormatDts", "DTS Multimedia File"));
    SupportedFileTypes.Add(TEXT("tta"), LOCTEXT("FormatTta", "True Audio Codec File"));
    SupportedFileTypes.Add(TEXT("tac"), LOCTEXT("FormatTac", "True Audio Codec File"));
    SupportedFileTypes.Add(TEXT("ty"), LOCTEXT("FormatTy", "TY Tivo File"));
    SupportedFileTypes.Add(TEXT("xa"), LOCTEXT("FormatXa", "XA File"));

    // Initialize supported URI schemes
    SupportedUriSchemes.Add(TEXT("http://"));
    SupportedUriSchemes.Add(TEXT("rtsp://"));

    // Register factory
    MediaModule->RegisterPlayerFactory(*this);

    UE_LOG(LogVlcMedia, Log, TEXT("Loaded VLC Plugin."));

    Initialized = true;
}

void FVlcMediaModule::ShutdownModule()
{
    if(!Initialized) {
        return;
    }

    Initialized = false;

    // Unregister video player factory
    IMediaModule* MediaModule = FModuleManager::GetModulePtr<IMediaModule>("Media");

    if(MediaModule != nullptr) {
        MediaModule->UnregisterPlayerFactory(*this);
    }

    // Shutdown VLC
    libvlc_release(vlc_instance);

    UE_LOG(LogVlcMedia, Log, TEXT("Unloaded VLC Plugin."));
}

TSharedPtr<IMediaPlayer> FVlcMediaModule::CreatePlayer()
{
    if(Initialized) {
        return MakeShareable(new FVlcMediaPlayer(vlc_instance));
    }

    return nullptr;
}

const FMediaFileTypes& FVlcMediaModule::GetSupportedFileTypes() const
{
    return SupportedFileTypes;
}

bool FVlcMediaModule::SupportsUrl(const FString& Url) const
{
    const FString Extension = FPaths::GetExtension(Url);

    if(!Extension.IsEmpty())
        return SupportedFileTypes.Contains(Extension);

    for(const FString& Scheme : SupportedUriSchemes) {
        if(Url.StartsWith(Scheme))
            return true;
    }

    return false;
}

bool FVlcMediaModule::LoadRequiredLibraries()
{
    if(dlopen("libvlc.so", RTLD_LAZY) == NULL) {
        UE_LOG(LogVlcMedia, Log, TEXT("Failed to load libvlc.so"));
        return false;
    }

    if(dlopen("libvlccore.so", RTLD_LAZY) == NULL) {
        UE_LOG(LogVlcMedia, Log, TEXT("Failed to load libvlccore.so"));
        return false;
    }

    return true;
}

#undef LOCTEXT_NAMESPACE

IMPLEMENT_MODULE(FVlcMediaModule, VlcMedia)
